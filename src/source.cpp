#include <header.hpp>

namespace ek
{
	bool summa(int m)
	{
		int a = m;
		int s = 0;
		std::vector<int> mas;
		while (a != 0)
		{
			mas.push_back(a % 10);
			a /= 10;
		}

		for (int i = 0; i < mas.size(); i++)
			s += mas[i];
		if (s == 18)
			return true;
		return false;
	}

	bool proizv(int m)
	{
		int a = m;
		int s = 1;
		std::vector<int> mas;
		while (a != 0)
		{
			mas.push_back(a % 10);
			a /= 10;
		}

		for (int i = 0; i < mas.size(); i++)
			s *= mas[i];
		if ((s % 35) == 0)
			return true;
		return false;
	}
}
